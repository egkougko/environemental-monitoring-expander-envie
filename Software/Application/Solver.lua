EQN_EPS = 1e-9

function IsZero(x)
  if x > -EQN_EPS AND x < EQN_EPS then zero = true
  else zero = false
  end
  return zero
end

function cbrt(x)
  if x > 0 then cbroot = math.pow(x, 1/3)
  else if x < 0 then cbroot = -math.pow(-x, 1/3)
  else cbroot = 0
  end
  return cbroot
end

---------------------- Solver for the qiadratic equation ----------------------
function SolveQuadric(c[3], s[2])
  p = c[1] / (2 * c[2])
  q = c[0] / c[2]
  D = p * p - q
  if IsZero(D) == true then
     s[0] = -p
     num =  1
  else if D < 0  then sol = 0
  else 
     sqrt_D = math.sqrt(D)
     s[0] = sqrt_D - p
     s[1] = -sqrt_D - p
     num = 2
  end
return num
end

---------------------- Solver for the cubic equation ----------------------
function SolveCubic(c[4], s[3])

  A = c[2] / c[3]
  B = c[1] / c[3]
  C = c[0] / c[3]

  -- substitute x = y - A/3 to eliminate quadric term: x^3 +px + q = 0
  sq_A = A * A
  p = 1.0 / 3 * (-1.0 / 3 * sq_A + B)
  q = 1.0 / 2 * (2.0 / 27 * A * sq_A - 1.0 / 3 * A * B + C)
  -- use Cardano's formula
  cb_p = p * p * p
  D = q * q + cb_p

  if IsZero(D) == true then
     if IsZero(q) == true then -- one triple solution
        s[0] = 0
        num = 1
     else -- one single and one double solution
        u = cbrt(-q)
        s[0] = 2 * u
        s[1] = -u
        num = 2
     end
  else if D < 0 -- Casus irreducibilis: three real solutions
     phi = 1.0 / 3 * math.acos(-q / math.sqrt(-cb_p))
     t = 2 * math.sqrt(-p)
     s[0] = t * math.cos(phi)
     s[1] = -t * math.cos(phi + math.pi / 3)
     s[2] = -t * math.cos(phi - math.pi / 3)
     num = 3
  else -- one real solution
     double sqrt_D = math.sqrt(D)
     u = cbrt(sqrt_D - q)
     v = -cbrt(sqrt_D + q)
     s[0] = u + v
     num = 1
  end

  sub = 1.0 / 3 * A
  for i = 1, num, 1 do s[i] -= sub end
  return num

end

---------------------- Solver for the quatric equation ----------------------
function SolveQuartic(c, s)

  A = c[3] / c[4]
  B = c[2] / c[4]
  C = c[1] / c[4]
  D = c[0] / c[4]

  -- substitute x = y - A/4 to eliminate cubic term x^4 + px^2 + qx + r = 0
  sq_A = A * A
  p = -3.0 / 8 * sq_A + B
  q = 1.0 / 8 * sq_A * A - 1.0 / 2 * A * B + C
  r = -3.0 / 256 * sq_A*sq_A + 1.0 / 16 * sq_A*B - 1.0 / 4 * A*C + D

  -- No absolute term: y(y^3 + py + q) = 0
  if IsZero(r) == true then
     coeffs[0] = q
     coeffs[1] = p
     coeffs[2] = 0
     coeffs[3] = 1
     num = SolveCubic(coeffs, s)
     for i = num+1, 4, 1 do s[i] = 0 end
  -- Solve the resolvent cubic ...
  else
     coeffs[0] = 1.0 / 2 * r * p - 1.0 / 8 * q * q
     coeffs[1] = -r
     coeffs[2] = -1.0 / 2 * p
     coeffs[3] = 1
     SolveCubic(coeffs, s)
      z = s[0] -- ... and take the one real solution ... 
      u = z * z - r -- ... to build two quadric equations 
      v = 2 * z - p  -- ... to build two quadric equations 
      if IsZero(u) then u = 0
      else if u > 0 then u = math.sqrt(u)
      else num = 0
      end
      if IsZero(v) then v = 0
      else if v > 0 then v = math.sqrt(v)
      else num = 0
      end
      coeffs[0] = z - u

      coeffs[1] = q < 0 ? -v : v

      coeffs[2] = 1
      num = SolveQuadric(coeffs, s)
      coeffs[0] = z + u

      coeffs[1] = q < 0 ? v : -v

      coeffs[2] = 1
      num += SolveQuadric(coeffs, s + num)
   end

  -- resubstitute
  sub = 1.0 / 4 * A
  for i = 0, num, +i do s[i] -= sub end
  return num
}