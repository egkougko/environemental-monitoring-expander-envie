-- Read all channels of a certain type
function ReadSensors (SensAdr, type)
  for g,t in pairs(SensAdd) do
    if type == "temp" then print("---------- Setting Humidity sensor " .. g .. " ----------") end
    else if type == "hum" then print("---------- Setting Temperature sensor " .. g .. " ----------") end
    for k4,v4 in pairs(multip) do 
      print("Writing pin " .. k4 .. " to " .. tostring(t[k4]))
      gpio.write(v4, t[k4])
    end
    print("ADC Value: ".. adc.read(0))

  end
end

-- Display Measurements
function DispMeas(scr, tbl, name, units)
  scr:clearBuffer()
  scr:setFont(u8g2.font_crox2cb_tf)
  scr:setFontPosCenter()
  scr:drawStr(6, 8, name)
  scr:drawBox(8, 16, 112, 2)
  scr:setFontPosBaseline()
  i = 0
  for l in pairs(tbl) do i = i + 1 end
  smdsp = { font1 = u8g2.font_crox2cb_tf, font 2 = u8g2.font_crox2c_tf, spx = 40, spy = 16}
  if i > 3 then 
    smdsp["font1"] = u8g2_font_timB08_tf
    smdsp["font2"] = u8g2.font_6x10_tf 
    smdsp["spx"] = 20
    smdsp["spy"] = 10
  end
  i = 1
  for l, m in pairs(tbl) do
     xpos = { spx1 = 1, spx2 = smdsp["spx"] }
     if i > 3 then
        xpos["spx1"] = 60
        xpos["spx2"] = 80
     end
     scr:setFont(smdsp["font1"])
     scr:drawStr(xpos["spx1"], 16 + i*smdsp["spy"], string.format("Ch%u%s", tonumber(string.match(l,"%d")), ":"))
     scr:setFont(smdsp["font2"])
     scr:drawStr(xpos["spx2"], 16 + i*smdsp["spy"], string.format("%.1f %s", m, unit))
     i = i + 1
  end
  scr:updateDisplay()
end

-- Convert ACD values to actual measuremetns
function CalcTemp(lvl, adctbl, Rreftbl)
  tmpv = -99
  mtrx = {}
  local e = (ADC["Vadc"]*lvl*Rref["RTDDiv"])/(ADC["Vcc"]*Rref["RTD0"]*ADC["lv"]-lvl*Rref["RTD0"]*ADC["Vadc"]) - 1
  if lvl >= 629 AND lvl < 721 then -- 4th degree solution with non-zero factors
     p_Qrt = (8*RTDCf["C"]*RTDCf["B"] - 3*((Rref["RTD0"]*RTDCf["C"])^2))/(8*(RTDCf["C"]^2))
     q_Qrt = ((-1*Rref["RTD0"]*RTDCf["C"])^3 + 4*Rref["RTD0"]*(RTDCf["C"]^2)*RTDCf["B"] + 8*(RTDCf["C"]^2)*RTDCf["A"] ) / (8*(RTDCf["C"]^3))
     discr0 = (RTDCf["B"]^2) + 3*RTDCf["C"]*RTDCf["A"]*Rref["RTD0"] - 12*RTDCf["C"]*e
     discr1 = 2*(RTDCf["B"]^3) + 9*RTDCf["A"]*RTDCf["B"]*RTDCf["C"]*Rref["RTD0"] - 
              27*((RTDCf["C"]*Rref["RTD0"])^2)*e + 27*RTDCf["C"]*(RTDCf["A"]^2) + 72*RTDCf["B"]*RTDCf["C"]*e
     discr = (4*(discr0^3)-(discr1^2))/27
     S_Qrt = 0
     Q_Qrt = 0
     -- Different cases
     if discr > 0 then -- case in which anyway discr0 > 0
        S_Qrt =1/2*math.sqrt(-(2/3)*p_Qrt + 2/(3*RTDCf["C"])*math.sqrt(discr0)*math.cos(math.acos(discr1/(2*math.sqrt(discr0^3)))/3) )
     else if discr < 0 
        if discr0 == 0 then
           S_Qrt = (1/2)*math.sqrt((-2/3)*p_Qrt + 1/(3*RTDCf["C"])*(discr1^(1/3) + discr0/(discr1^(1/3))))
        else
           Q_Qrt = (discr1 + math.sqrt((discr1^2)+4*(discr0^3)))^(1/3)
           S_Qrt = (1/2)*math.sqrt((-2/3)*p_Qrt + (1/(3*RTDCf["C"]))*(q_Qrt+(discr0/Q_Qrt)))
           if S_Qrt == 0 then -- we have to change the Q cubic root,is solution undefiend?

           end
        end
     else if discr == 0 
        if discr0 == 0 then -- this is the reductible quatric, solutions are simpler

        else
           Q_Qrt = (discr1 + math.sqrt((discr1^2)+4*(discr0^3)))^(1/3)
           S_Qrt = (1/2)*math.sqrt((-2/3)*p_Qrt + (1/(3*RTDCf["C"]))*(q_Qrt+(discr0/Q_Qrt)))
        end
     end
     mtrx = {
             ((Rref["RTD0"]*RTDCf["C"])/(4*RTDCf["C"])) - S_Qrt + (1/2)*sqrt(-4*(S_Qrt^2)-2*p_Qrt+(q_Qrt/S_Qrt)),
             ((Rref["RTD0"]*RTDCf["C"])/(4*RTDCf["C"])) - S_Qrt - (1/2)*sqrt(-4*(S_Qrt^2)-2*p_Qrt+(q_Qrt/S_Qrt)),
             ((Rref["RTD0"]*RTDCf["C"])/(4*RTDCf["C"])) + S_Qrt + (1/2)*sqrt(-4*(S_Qrt^2)-2*p_Qrt+(q_Qrt/S_Qrt)),
             ((Rref["RTD0"]*RTDCf["C"])/(4*RTDCf["C"])) + S_Qrt - (1/2)*sqrt(-4*(S_Qrt^2)-2*p_Qrt+(q_Qrt/S_Qrt))
            }
    for i = 1, #mtrx do
       if mtrx[i] < 0 AND mtrx[i] >= -55 
         tmpv = mtrx[i] 
       end
    end
  else if lvl >= 721 AND lvl < 918 -- 2nd degree solution
    discr = RTDCf["A"]^2+4*RTDCf["B"]*e
    if discr > 0 then
      mtrx = {(-RTDCf["A"] + math.sqrt(discr))/2*RTDCf["B"], (-RTDCf["A"] - math.sqrt(discr))/2*RTDCf["B"]}
      for i = 1, #mtrx do
         if mtrx[i] >= 0 AND mtrx[i] <= 175 
           tmpv = mtrx[i] 
         end
      end
    end
    else if discr = 0 then tmpv = -RTDCf["A"]/2*RTDCf["B"] end
  end
  return tmpv
end



---------------------------------------------------- WebServer Application ----------------------------------------------------

-- ESP8266 webserver
-- This allows control of the builtin LED - On/Off/Blink/Blink Off.
--
local outpin=4
local state = false

function init()
  gpio.mode(outpin,gpio.OUTPUT)
  gpio.write(outpin,gpio.HIGH)  -- LED is pulled so HIGH = off
  blinkOFF()
end

function togLED()
  if state==false then gpio.write(outpin,gpio.HIGH)
  else                 gpio.write(outpin,gpio.LOW) end
  state = not state;
end

function blkinkON()
  if mytimer~=nil then return end -- Timer already on.
  mytimer = tmr.create()
  mytimer:alarm(200, tmr.ALARM_AUTO, function()  togLED() end)
end

function blinkOFF()
  if mytimer==nil then return end -- Timer already off.
  mytimer:unregister()  mytimer=nil
end

srv=net.createServer(net.TCP)
init()
    srv:listen(80,function(conn)
    conn:on("receive",function(conn,payload)
    print(payload)  -- View the received data,

    function controlLED()
      control = string.sub(payload,fnd[2]+1) -- Data is at end already.
      if control == "ON"       then gpio.write(outpin,gpio.LOW);  blinkOFF() return end
      if control == "OFF"      then gpio.write(outpin,gpio.HIGH); blinkOFF() return end
      if control == "Blink"    then blkinkON() return end
      if control == "Blinkoff" then blinkOFF() return end
    end

    --get control data from payload
    fnd = {string.find(payload,"ledbi=")}
    if #fnd ~= 0 then controlLED() end -- Is there data in payload? - Take action if so.

    conn:send('<!DOCTYPE HTML>\n')
    conn:send('<html>\n')
    conn:send('<head><meta http-equiv="content-type" content="text/html; charset=UTF-8">\n')
    -- Scale the viewport to fit the device.
    conn:send('<meta name="viewport" content="width=device-width, initial-scale=1">')
    -- Title
    conn:send('<title>ESP8266 Wifi LED Control</title>\n')
    -- CSS style definition for submit buttons
    conn:send('<style>\n')
    conn:send('input[type="submit"] {\n')
    conn:send('color:#050; width:70px; padding:10px;\n')
    conn:send('font: bold 84% "trebuchet ms",helvetica,sans-serif;\n')
    conn:send('background-color:lightgreen;\n')
    conn:send('border:1px solid; border-radius: 12px;\n')
    conn:send('transition-duration: 0.4s;\n')
    conn:send('}\n')
    conn:send('input[type="submit"]:hover {\n')
    conn:send('background-color:lightblue;\n')
    conn:send('color: white;\n')
    conn:send('}')
    conn:send('</style></head>\n')
    -- HTML body Page content.
    conn:send('<body>')
    conn:send('<h1>Control of nodeMCU<br>(ESP8266-E12) Built in LED.</h1>\n')
    conn:send('<p>The built in LED for NodeMCU V3 is on D4</p>\n')
    -- HTML Form (POST type) and buttons.
    conn:send('<form action="" method="POST">\n')
    conn:send('<input type="submit" name="ledbi" value="ON" > Turn Built in LED on<br><br>\n')
    conn:send('<input type="submit" name="ledbi" value="OFF"> Turn Built in LED off<br><br>\n')
    conn:send('<input type="submit" name="ledbi" value="Blink"> Blink LED<br><br>\n')
    conn:send('<input type="submit" name="ledbi" value="Blinkoff"> Stop LED Blink</form>\n')
    conn:send('</body></html>\n')
    conn:on("sent",function(conn) conn:close() end)
    end)
end)

---------------------------------------------------- Led Blink  ----------------------------------------------------
local LEDpin = 4
local LEDState = false

local n = 0
local _nStart = 0
local _togDelay
local _interval

local blinkTimer = tmr.create()
local blinkInterval = tmr.create()

gpio.mode(4,gpio.OUTPUT)
gpio.write(LEDpin,gpio.HIGH)   -- high is off for builtin LED.

-- Blink  n times with delay between on-off then wait interval - repeat
function LEDBlinkN(numTog,togDelay,interval)   -- Get round not having statics
  if blinkTimer:state() ~= nil then LEDBlinkStop() end-- Stop if was previously started.
  _nStart   = numTog
  _togDelay = togDelay
  _interval = interval
  doInterval()
end

function LEDBlinkStop()
  if blinkTimer:state() ~= nil  then
    blinkTimer:unregister()
    blinkInterval:unregister()
  end
  gpio.write(LEDpin,gpio.HIGH)
  LEDState = false
  n = 0
end

function toggleLEDbuiltin()
  if blinkTimer:state() ~= nil and n <=0 then blinkTimer:unregister() return end
   if (LEDstate) then
     gpio.write(LEDpin,gpio.LOW)
   else
     gpio.write(LEDpin,gpio.HIGH)   -- high is off for builtin LED.
     n = n -1
   end
   LEDstate = not LEDstate
end

-- Fast toggle
function LEDBlinkNStart()
  n = _nStart -- Initialise the toggle number
  blinkTimer:alarm(_togDelay, tmr.ALARM_AUTO, function () toggleLEDbuiltin() end)
end

-- Interval timer
function doInterval()
  LEDBlinkNStart()  -- Start with the led toggle before the interval
  blinkInterval:alarm(_interval, tmr.ALARM_AUTO, function () LEDBlinkNStart() end)
end

--test
--LEDBlinkN_setNumBlinks(50,4)
--LEDBlinkN(3,50,1500)

---------------------------------------------------- lua.init  ----------------------------------------------------

--nodemcu_test_startup
-- load credentials, 'SSID' and 'PASSWORD' declared and initialize in there
dofile("credentials.lua")
dofile("LEDBlinkn.lua")

function startup()
  if file.open("init.lua") == nil then
    print("init.lua deleted or renamed")
  else
    print("Running")
    file.close("init.lua")
    LEDBlinkStop()
    file.close("LEDBlinkn.lua")
    -- the actual application is stored in 'application.lua'
    dofile("application.lua")
  end
end

-- Define WiFi station event callbacks
wifi_connect_event = function(T)
  print("Connection to AP("..T.SSID..") established!")
  print("Waiting for IP address...")
  if disconnect_ct ~= nil then disconnect_ct = nil end
  LEDBlinkN(1,100,200) -- faster blinks
end

wifi_got_ip_event = function(T)
  -- Note: Having an IP address does not mean there is internet access!
  -- Internet connectivity can be determined with net.dns.resolve().
  print("Wifi connection is ready! IP address is: "..T.IP)
  print("Startup will resume momentarily, you have 3 seconds to abort.")
  print("Waiting...")
  startUpTimer = tmr.create() -- JFM mod to allow abort
  startUpTimer:alarm(3000, tmr.ALARM_SINGLE, startup)

  LEDBlinkN(3,50,550) -- fast warning 3, delay
end

function stopWiFi() -- JFM
   LEDBlinkStop()
   startUpTimer:stop()
   startUpTimer:unregister()
end

wifi_disconnect_event = function(T)

  LEDBlinkN(2,100,700) -- double flash with delay = error

  if T.reason == wifi.eventmon.reason.ASSOC_LEAVE then
    --the station has disassociated from a previously connected AP
    return
  end
  -- total_tries: how many times the station will attempt to connect to the AP. Should consider AP reboot duration.
  local total_tries = 75
  print("\nWiFi connection to AP("..T.SSID..") has failed!")

  --There are many possible disconnect reasons, the following iterates through
  --the list and returns the string corresponding to the disconnect reason.
  for key,val in pairs(wifi.eventmon.reason) do
    if val == T.reason then
      print("Disconnect reason: "..val.."("..key..")")
      break
    end
  end

  if disconnect_ct == nil then
    disconnect_ct = 1
  else
    disconnect_ct = disconnect_ct + 1
  end
  if disconnect_ct < total_tries then
    print("Retrying connection...(attempt "..(disconnect_ct+1).." of "..total_tries..")")
  else
    wifi.sta.disconnect()
    print("Aborting connection to AP!")
    disconnect_ct = nil
  end
end

-- Register WiFi Station event callbacks
wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)

LEDBlinkN(1,200,400) -- Idle
print("Connecting to WiFi access point...")
wifi.setmode(wifi.STATION)
wifi.sta.config({ssid=SSID, pwd=PASSWORD})
-- wifi.sta.connect() not necessary because config() uses auto-connect=true by default

