-- SetUp basic Configuration
 dofile("Configuration.lua")

 disp:drawRFrame(0,0,128,64,7)
 disp:drawRFrame(1,1,126,62,7)
 disp:setFont(u8g2.font_fub30_tr)
 disp:drawStr(10,47,"EnviE")
 disp:updateDisplay()

 -- Connect to WiFi from credintials (SSID, PASSWORD) or switch to access point mode
function WiFiSetup(CmpMtr)
  disp:clearBuffer()
  disp:setFont(u8g2.font_6x10_tf)
  disp:drawStr(0,10,"Starting WiFi...")
  disp:updateDisplay()
  if file.open("Credentials.lua") == nil then
    print("WiFi not set, switching to AP mode...")
    wifi.setmode(wifi.SOFTAP)
    disp:drawStr(0,20,"No Credentials found,")
    disp:drawStr(0,30,"   setting AP mode...")
    disp:updateDisplay()
    wifi.ap.config(ssid_cfg)
    wifi.ap.setip(ip_cfg)
    disp:drawStr(0,40,"Starting DHCP...")
    disp:updateDisplay()
    wifi.ap.dhcp.start()
  else
    print("Connecting to WiFi access point...")
    file.close("credentials.lua")
    dofile("Credentials.lua")
    wifi.setmode(wifi.STATION)
    wifi.sta.config(ssid_cfg)
    wifi.sta.setip(ip_cfg)
    end
  disp:drawStr(0,50,"WiFi Setup Complete!")
  disp:updateDisplay()
  CmpMtr["WiFi"] = true
end

-- Helper function to trigger event execution
function RunNext(var, timer, func, CmpMtr)
  if var then
     tmr.create():alarm(3000, tmr.ALARM_SINGLE, function() func(CmpMtr) end)
     timer:stop()
     timer:unregister()
  end
end

-- Function to run the app
AppRun = function(CmpMtr)
  if file.open("init.lua") == nil then
    print("init.lua deleted or renamed")
    CmpMtr["App"] = false
  else
    print("Running")
    file.close("init.lua")
    dofile("Application.lua")
    CmpMtr["App"] = true
  end
end

-- Define WiFi station event callbacks
wifi_connect_event = function(T)
  print("Connection to AP("..T.SSID..") established!")
  print("Waiting for IP address...")
  if disconnect_ct ~= nil then disconnect_ct = nil end
end

wifi_got_ip_event = function(T)
  print("Wifi connection is ready! IP address is: "..T.IP)
end

wifi_disconnect_event = function(T)
  if T.reason == wifi.eventmon.reason.ASSOC_LEAVE then
    return
  end
  local total_tries = 75
  print("\nWiFi connection to AP("..T.SSID..") has failed!")
  for key,val in pairs(wifi.eventmon.reason) do
    if val == T.reason then
      print("Disconnect reason: "..val.."("..key..")")
      break
    end
  end
  if disconnect_ct == nil then disconnect_ct = 1
  else disconnect_ct = disconnect_ct + 1
  end
  if disconnect_ct < total_tries then
    print("Retrying connection...(attempt "..(disconnect_ct+1).." of "..total_tries..")")
  else
    wifi.sta.disconnect()
    print("Aborting connection to AP!")
    disconnect_ct = nil
  end
end

-- Register WiFi Station event callbacks
wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)

print("Waiting...")

tmr.create():alarm(3000, tmr.ALARM_SINGLE, function() WiFiSetup(done) end)

local WiFiTmr = tmr.create()
WiFiTmr:register(400, tmr.ALARM_AUTO, function () RunNext(done["WiFi"], WiFiTmr, AppRun, done) end)
WiFiTmr:start()

-- monitor an input pin with output to the terminal
-- function monitor( pin )
--  uart.write( 0, ''..gpio.read(pin) )
-- end
-- gpio.mode(9,gpio.INPUT,gpio.PULLUP)
-- tmr.create():alarm( 500,1,fuction() monitor(9) end )