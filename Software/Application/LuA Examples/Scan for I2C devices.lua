-- http://www.esp8266.com/viewtopic.php?f=19&t=771
-- Scan for I2C devices

id=0
sda=2
scl=1

-- initialize i2c, set pin1 as sda, set pin0 as scl

i2c.setup(id,sda,scl,i2c.SLOW)

for i=0,127 do 
  i2c.start(id) 
  resCode = i2c.address(id, i, i2c.TRANSMITTER) 
  i2c.stop(id) 
  add = "0x" .. string.format("%02x", i) .. " (" .. i .. ")" 
  if resCode == true then print("We have a device on address " .. add) else print("Nothing on adress " .. add) end 
end

-- Pins to set status for the multiplexer

sc0=0
sc1=3
sc2=4

-- Selection table for the different channels

temp1 = {
  sc0 = gpio.LOW,
  sc1 = gpio.LOW,
  sc2 = gpio.LOW
}
temp2 = {
  sc0 = gpio.HIGH,
  sc1 = gpio.LOW,
  sc2 = gpio.LOW
}
temp3 = {
  sc0 = gpio.LOW,
  sc1 = gpio.HIGH,
  sc2 = gpio.LOW
}
temp4 = {
  sc0 = gpio.HIGH,
  sc1 = gpio.HIGH,
  sc2 = gpio.LOW
}
temp5 = {
  sc0 = gpio.LOW,
  sc1 = gpio.LOW,
  sc2 = gpio.HIGH
}
hum1 = {
  sc0 = gpio.HIGH,
  sc1 = gpio.LOW,
  sc2 = gpio.HIGH
}
hum2 = {
  sc0 = gpio.LOW,
  sc1 = gpio.HIGH,
  sc2 = gpio.HIGH
}
hum3 = {
  sc0 = gpio.HIGH,
  sc1 = gpio.HIGH,
  sc2 = gpio.HIGH
}

-- gpio mode setting

gpio.mode(sc0, gpio.OUTPUT)
gpio.mode(sc1, gpio.OUTPUT)
gpio.mode(sc2, gpio.OUTPUT)

gpio.write(sc0, temp1.sl0)
gpio.write(sc1, temp1.sl1)
gpio.write(sc2, temp1.sl2)

-- ADC commands

print(" "..adc.read(0))
print(" "..adc.readvdd33())
adc.force_init_mode(adc.INIT_VDD33)

-- Screen 

sla = 0x3c
disp = u8g2.ssd1306_i2c_128x64_noname(id, sla)
disp:drawBox(0,0,128,64)
disp:updateDisplay()
disp:clearBuffer()
disp:updateDisplay()




