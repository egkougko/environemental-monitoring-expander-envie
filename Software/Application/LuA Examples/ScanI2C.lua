-- http://www.esp8266.com/viewtopic.php?f=19&t=771
-- Scan for I2C devices

id=0
sda=4
scl=5

-- initialize i2c, set pin1 as sda, set pin0 as scl

i2c.setup(id,sda,scl,i2c.SLOW)

for i=0,255 do 
  i2c.start(id) 
  resCode = i2c.address(id, i, i2c.TRANSMITTER) 
  i2c.stop(id) 
  add = "0x" .. string.format("%02x", i) .. " (" .. i .. ")" 
  if resCode == true then print("We have a device on address " .. add) else print("Nothing on adress " .. add) end 
end

