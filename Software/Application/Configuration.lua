-- General Variables
done = { WiFi = false, App = false, Webpage = false} 
RTDCf = { A = 3,91E-03, B = -5,78E-07, C = -4,18E-12 }
ADC = { Vadc = 3.3, lv = 1023, Vcc = 5 }
Rref = { RTD0 = 100, RTDDiv = 115}

-- initialize i2c, set pin1 as sda, set pin0 as scl
id = 0
sda = 2
scl = 1
i2c.setup(id,sda,scl,i2c.SLOW)

-- Initialize Screen 
sla = 0x3c
disp = u8g2.ssd1306_i2c_128x64_noname(id, sla)
disp:setFlipMode(1)

-- Pins to set status for the multiplexer
multip = {  sc0 = 0,  sc1 = 3,  sc2 = 4 }
for k3,v3 in pairs(multip) do gpio.mode(v3, gpio.OUTPUT) end

-- Temp Sensor Table
RTDs = {
        temp1 = { sc0 = gpio.LOW, sc1 = gpio.LOW, sc2 = gpio.LOW },
        temp2 = { sc0 = gpio.HIGH, sc1 = gpio.LOW, sc2 = gpio.LOW },
        temp3 = { sc0 = gpio.LOW, sc1 = gpio.HIGH, sc2 = gpio.LOW },
        temp4 = { sc0 = gpio.HIGH, sc1 = gpio.HIGH, sc2 = gpio.LOW },
        temp5 = { sc0 = gpio.LOW, sc1 = gpio.LOW, sc2 = gpio.HIGH }
}

-- Humidity Sensor Table
HIHs = { hum1 = { sc0 = gpio.HIGH, sc1 = gpio.LOW, sc2 = gpio.HIGH },
         hum2 = { sc0 = gpio.LOW, sc1 = gpio.HIGH, sc2 = gpio.HIGH },
         hum3 = { sc0 = gpio.HIGH, sc1 = gpio.HIGH, sc2 = gpio.HIGH }
}

-- Measurements Table
temps = { temp1 = -99, temp2 = -99, temp3 = -99, temp4 = -99, temp5 = -99 }
humids = { hum1 = -99, hum2 = -99, hum3 = -99 }

-- ADC Initialize commands
adc.force_init_mode(adc.INIT_VDD33)

--Wifi AP Comgifuration (60:01:94:70:1e:87)
ip_cfg = {
          ip="192.168.1.1",
          netmask="255.255.255.0",
          gateway="192.168.1.1"
}
ssid_cfg = {
            ssid="EnviE",
            pwd="EnviE-CERN"
}
